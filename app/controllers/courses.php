<?php



class Courses
{

    const COURSE_PUBLIC_FIELDS = [
        'id', 'title', 'shortDescription', 'longDescription', 'technologies',
        'startDate', 'imageUrl', 'locationId', 'locationName',
        'location', 'status', 'joinPolicy', 'motivationLabel'
    ];


    function __construct()
    {
        echo "We are in Courses<br>";
    }

    public function getCourses($query)
    {
        return $query;
    }

    public function other($args = false){
        echo 'Inside other<br>';
        if($args){
            echo 'Option: ' . $args . '<br>';
        }


        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//        echo $url . '<br>';

        $query = parse_url($url, PHP_URL_QUERY);
//        echo $query . '<br>';
        parse_str($query, $params);
        foreach ($params as $key => $value){
            echo $value . '<br>';
        }
    }
}
