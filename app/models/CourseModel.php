<?php

class CourseModel
{

    private $_id;
    private $_title;
    private $_description;
    private $_startDate;
    private $_visible;

    public function __construct($id, $title, $description, $startDate, $visible)
    {
        $this->setID($id);
        $this->setTitle($title);
        $this->setDescription($description);
        $this->setStartDate($startDate);
        $this->setVisible($visible);
    }

    public function getID(){
        return $this->_id;
    }

    public function getTitle(){
        return $this->_title;
    }

    public function getDescription(){
        return $this->_description;
    }

    public function getStartDate(){
        return $this->_startDate;
    }

    public function getVisible(){
        return $this->_visible;
    }

    public function setID($id){
        $this->_id = $id;
    }

    public function setTitle($title){
        $this->_title = $title;
    }

    public function setDescription($description){
        $this->_description = $description;
    }

    public function setStartDate($startDate){
        $this->_startDate = $startDate;
    }

    public function setVisible($visible){

        if(strtoupper($visible) !== 'Y' && strtoupper($visible) !== 'N'){
            throw new Exception("Course must be Y or N");
        }
        $this->_visible = $visible;
    }

    public function returnCourseAsArray(){
        $course = array();
        $course['id']= $this->getID();
        $course['title'] = $this->getTitle();
        $course['description'] = $this->getDescription();
        $course['deadline'] = $this->getStartDate();
        $course['completed'] = $this->getVisible();
        return $course;
    }

}
